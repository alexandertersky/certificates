<?php


namespace Src;


class Counter
{
    public $count;
    public $progress = 1;
    public $per;
    public $iterable_counter = 0;

    public function __construct($array)
    {
        $this->per = count($array) / 100;
        $this->showProgress();
    }

    public function iteration()
    {
        $this->iterable_counter++;
        if ($this->iterable_counter > $this->per * $this->progress) {
            self::erase();
            $this->progress++;
            $this->showProgress();
        }
    }

    public function showProgress()
    {
        echo $this->progress . '%';
    }

    public static function erase()
    {
        echo "\r\033[K";
    }
}