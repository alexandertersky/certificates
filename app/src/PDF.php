<?php

namespace Src;

class PDF
{
    public $encoding = 'cp1251';
    public $text;
    public $pdf;
    public $font = 'times';
    public $style = '';
    public $font_size;
    public $color = [0, 0, 0];
    public $margin_top;
    public $margin_left;

    public function __construct($orientation = 'P', $unit = 'mm', $size = 'A4')
    {
        $this->pdf = new \FPDF($orientation, $unit, $size);
        $this->pdf->AddFont('segoe', '', 'segoe.php');
        $this->pdf->AddFont('segoe', 'I', 'segoei.php');
        $this->pdf->AddFont('segoe', 'B', 'segoeb.php');
        $this->pdf->AddFont('segoe', 'BI', 'segoebi.php');
        $this->pdf->SetFont($this->font, $this->style);

        $this->pdf->SetAutoPageBreak(false);
    }

    public function __get($name)
    {
        return $this->$name();
    }

    public function text(string $text)
    {
        $this->text = mb_convert_encoding($text, $this->encoding);
        return $this;
    }

    public function text_width()
    {
        return $this->pdf->GetStringWidth($this->text);
    }

    public function center()
    {
        $x = $this->pdf->GetPageWidth() / 2 - ($this->pdf->GetStringWidth($this->text) / 2);
        $this->margin_left = $x;
        $this->pdf->SetX($x);
        return $this;
    }

    public function right()
    {
        $x = $this->pdf->GetPageWidth() - $this->pdf->GetStringWidth($this->text)-15;
        $this->margin_left = $x;
        $this->pdf->SetX($x);
        return $this;
    }

    public function marginTop($y)
    {
        $this->margin_top = $y;
        $this->pdf->setY($y);
        return $this;
    }

    public function size($size)
    {
        $this->pdf->SetFontSize($size);
        $this->font_size = $size;
        return $this;
    }

    public function color($r, $g, $b)
    {
        $this->color = [$r, $g, $b];
        $this->pdf->SetTextColor($this->color[0], $this->color[1], $this->color[2]);
        return $this;
    }

    public function font($font)
    {
        $this->font = $font;
        $this->pdf->SetFont($this->font, $this->style);
    }

    public function bold()
    {
        $this->style = $this->style === 'I' ? 'BI' : 'B';
        $this->pdf->SetFont($this->font, $this->style);
        return $this;
    }

    public function italic()
    {
        $this->style = $this->style === 'B' ? 'BI' : 'I';
        $this->pdf->SetFont($this->font, $this->style);
        return $this;
    }

    public function underlined($start_offset = 0, $end_offset = null, $y_diff = 0, $thickness = 1)
    {
        $y = $this->margin_top + ($this->font_size * 0.352778 / 2) - $y_diff;
        $start = $this->margin_left;
        $end = $this->margin_left + $this->text_width;

        if ($start_offset > 0) {
            $all_length = strlen($this->text);
            $c = $this->text_width / $all_length;

            $words = explode(' ', $this->text);

            for ($i = 0; $i < $start_offset; $i++) {
                $start += ((strlen($words[$i]) + .6) * $c);
            }

        }
        for ($i = 0; $i< $thickness; $i++){
            $offset_y = $i*0.1;
            $this->pdf->Line($start, $y+$offset_y, $end, $y+$offset_y);
        }
        return $this;
    }


    public function page()
    {
        $this->pdf->AddPage();
    }

    public function write()
    {
        $this->pdf->Write(1, $this->text);
        $this->clear();
    }

    public function save($filename)
    {
        if (is_file($filename)) {
            //todo rewrite document?
            unlink($filename);
        }
        $this->pdf->Output($filename, 'F');
    }

    public function background($path)
    {
        $this->pdf->Image($path, 0, 0, $this->pdf->GetPageWidth(), $this->pdf->GetPageHeight());
    }

    public function image($path, $float, $y, $scale = 1)
    {
        $w = $this->pdf->GetPageWidth() * $scale;
        switch ($float) {
            case 'left':
                $x = 0;
                break;
            case 'center':
                $x = $this->pdf->GetPageWidth() / 2 - $w / 2;
                break;
            case 'right':
                $x = $this->pdf->GetPageWidth() - $w;
        }
        $this->pdf->Image($path, $x, $y, $w);
    }

    public function clear()
    {
        $this->pdf->SetTextColor(0, 0, 0);
        $this->pdf->SetFont($this->font, '');
        $this->style = '';
    }
}