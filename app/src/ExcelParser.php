<?php

namespace Src;


class ExcelParser
{
    static $path = DIR . '/';

    public static function parse($src)
    {
        $src = static::$path . $src;
        if (!file_exists($src)) {
            die(static::$path);
        }

        self::extractZip($src);

        $data = self::parseXLS();

        return $data;
    }

    protected static function extractZip($src)
    {
        $zip = new \ZipArchive();
        $zip->open($src);

        $zip->extractTo(self::$path . '.tmp/');
        $zip->close();
    }

    protected static function parseXLS()
    {
        $xml = simplexml_load_file(self::$path . '.tmp/xl/sharedStrings.xml');
        $sharedStringsArr = [];
        foreach ($xml->children() as $item) {
            $sharedStringsArr[] = (string)$item->t;
        }

        $handle = @opendir(self::$path . '.tmp/xl/worksheets');

        $out = [];
        while ($file = @readdir($handle)) {
//            проходим по всем файлам из директории /xl/worksheets/
            if ($file != "." && $file != ".." && $file != '_rels') {
                $xml = simplexml_load_file(self::$path . '.tmp/xl/worksheets/' . $file);
//                по каждой строке
                $row = 0;
                foreach ($xml->sheetData->row as $item) {
                    $out[$file][$row] = [];
//                    по каждой ячейке строки
                    $cell = 0;
                    foreach ($item as $child) {
                        $attr = $child->attributes();
                        $value = isset($child->v) ? (string)$child->v : false;
                        $out[$file][$row][$cell] = isset($attr['t']) ? $sharedStringsArr[$value] : $value;
                        $cell++;
                    }
                    $row++;
                }
            }
        }
        return array_shift($out);
    }

    public static function removeTmp($dir)
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (is_dir($dir . "/" . $object))
                        self::removeTmp($dir . "/" . $object);
                    else
                        unlink($dir . "/" . $object);
                }
            }
            rmdir($dir);
        }
    }
}
