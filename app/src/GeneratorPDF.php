<?php

namespace Src;

class GeneratorPDF
{

    public static function generate($records)
    {
        $pdf = new PDF();
        $date = date('d_m_Y');
        $directory = DIR . '/сертификаты_' . $date;

        if (!is_dir($directory)) {
            if (!mkdir($directory) && !is_dir($directory)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $directory));
            }
        }

        $counter = new Counter($records);
        foreach ($records as $record) {
            $pdf->page();
            self::writePage($pdf, $record);

            $single_pdf = new PDF();
            $single_pdf->page();
            self::writePage($single_pdf, $record);

            $single_pdf->save($directory . '/' . $record[0].'.pdf');
            $counter->iteration();
        }
        echo PHP_EOL;

        $pdf->save(DIR . '/все сертификаты_' . $date . '.pdf');
    }

    public static function writePage(PDF $pdf, $record)
    {
        if (isset($record[0]) && isset($record[3]) && isset($record[4])) {
            $pdf->background(__DIR__ . '/../assets/background.jpg');
            $pdf->image(__DIR__ . '/../assets/logo.png', 'center', 21, 0.55);

            $pdf->font('segoe');

            $pdf->text('Сертификат')->size(42)->marginTop(102)->center()->write();
            $pdf->text('000155')->size(19)->marginTop(113.5)->bold()->color(255, 0, 0)->center()->write();
            $pdf->text('Данный сертификат подтверждает')->size(20)->marginTop(121)->italic()->center()->write();
            $pdf->text($record[0])->size(39)->marginTop(134)->center()->italic()->underlined(0, null, 1.5, 2)->write();
            $pdf->text($record[4])->size(18)->marginTop(151)->italic()->center()->write();
            $pdf->text('принял(а) участие в конкурсе')->size(18)->marginTop(164)->italic()->center()->write();
            $pdf->text('Секреты здоровья')->size(24)->marginTop(191)->bold()->center()->write();
            $pdf->text('руководитель: ' . $record[3])->size(18)->marginTop(217)->italic()->center()->underlined(1, 0, 0.6)->write();
            $pdf->text('Председатель оргкоммитета:')->size(15)->marginTop(255)->italic()->bold()->write();
            $pdf->text('В.С. Осипов')->size(15)->marginTop(255)->italic()->bold()->right()->write();
            $pdf->text('2019 г.')->size(15)->marginTop(281)->italic()->center()->write();

        }
    }

}