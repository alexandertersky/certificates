<?php
/** @noinspection ForgottenDebugOutputInspection */

function ddd()
{
    $args = func_get_args();
    foreach ($args as $arg) {
        print_r($arg);
        echo PHP_EOL;
    }
    $backtrace = debug_backtrace()[0];
    echo $backtrace['file'] . ' on line ' . $backtrace['line'];
    echo PHP_EOL;
    exit();
}

const VERSION = 'debug';

$dir = VERSION === 'debug' ? __DIR__ . '/../' : dirname(Phar::running(false));

$GLOBALS['DIR'] = $dir;
define('DIR', $dir);

if (file_exists(DIR . '/Export.xlsx')) {
    $res = 'Yes';
}

require_once __DIR__ . '/vendor/autoload.php';

use Bin\App;

$app = new App($argv);
$app->run();

if (VERSION === 'debug'){
    $f = explode('/', __FILE__);
    array_pop($f);
    $dir = DIR . array_pop($f);
    $output = shell_exec('php ' . $dir . '/app.php remove');
    die($output.PHP_EOL);
} else {
    $f = explode('/', __FILE__);
    array_pop($f);
    $dir = DIR . '/' . array_pop($f);
    $output = shell_exec('php ' . $dir . ' remove');
}
