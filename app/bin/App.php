<?php

namespace Bin;

use Src\ExcelParser;
use Src\GeneratorPDF;

class App
{
    protected $args;
    protected $excel;

    public function __construct($argv)
    {
        array_shift($argv);
        $this->args = $argv;
    }

    /**
     * Запуск приложения
     */
    public function run()
    {
        $file = $this->args[0];

        if ($file == 'remove') {
            ExcelParser::removeTmp(DIR . '/.tmp');
            die('.tmp removed');
        } else {
            $records = ExcelParser::parse($file);
            array_shift($records);
            GeneratorPDF::generate($records);
        }
    }


}